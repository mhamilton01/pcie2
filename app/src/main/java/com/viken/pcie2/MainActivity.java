package com.viken.pcie2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        final TextView tv = findViewById(R.id.sample_text);
        tv.setText("Starting");
        map("/sys/bus/pci/devices/0002:01:00.0");

        Button map = findViewById(R.id.map);
        map.setOnClickListener(new View.OnClickListener() {
            // chmod a+rw /sys/bus/pci/devices/0002:01:00.0/resource0
            // chmod a+rw /sys/bus/pci/devices/0002:01:00.0/resource2
            @Override
            public void onClick(View v) {
                map("/sys/bus/pci/devices/0002:01:00.0");
            }
        });
        Button read = findViewById(R.id.read);
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "";
                for (int i = 0; i < 16; i++) {
                    int value = readIntReg(i);
                    msg += "register " + i + " = 0x" + Integer.toHexString(value) + "\n";
                }
                tv.setText(msg);
            }
        });
        Button readShort = findViewById(R.id.readshort);
        readShort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "";
                for (int i = 0; i < 16; i++) {
                    int value = readShortReg(i);
                    msg += "register " + i + " = 0x" + Integer.toHexString(value) + "\n";
                }
                tv.setText(msg);
            }
        });
        Button readMem = findViewById(R.id.readmem);
        readMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "";
                for (int i = 0; i < 16; i++) {
                    int value = readIntMem(i);
                    msg += "register " + i + " = 0x" + Integer.toHexString(value) + "\n";
                }
                tv.setText(msg);
            }
        });
        Button writeMem = findViewById(R.id.writemem);
        writeMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                for (int i = 0; i < 16; i++) {
                    writeIntMem(rand.nextInt(Integer.MAX_VALUE), i);
                }
                tv.setText("random numbers written to mem");
            }
        });
        Button testMem = findViewById(R.id.test);
        testMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starting();
                test(1);
            }
        });
        Button testProc = findViewById(R.id.test_proc);
        testProc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starting();
                testProc(1);
            }
        });
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void map(String resourceBase);
    public native int readIntReg(int regOffset);
    public native int readShortReg(int regOffset);
    public native int readIntMem(int regOffset);
    public native int readShortMem(int regOffset);
    public native void writeIntMem(int data, int regOffset);
    public native void writeShortMem(short data, int regOffset);
    public native void test(int data);
    public native void testProc(int data);
    public native void starting();
}

#include <jni.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <chrono>
#include <sys/stat.h>
#include "android/log.h"

#define LOG_TAG "native-lib"
#define DLOG(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

static int res0fd;
static int res2fd;
static void *map0;
static void *map2;

static int openResource(const char *bar) {
    /* Open PCI resource files */
    int resfd = open(bar, O_RDWR);
    if (resfd < 0) {
        DLOG("error opening resource file %s: %s\n", bar, strerror(errno));
        return errno;
    }
    DLOG("opened PCI resource: %s\n", bar);
    return resfd;
}

static int barSize(int fd) {
    struct stat sb;
    if (fstat(fd, &sb) == -1) {
        DLOG("error fstat resource file fd %d: %s\n", fd, strerror(errno));
        return 0;
    }
    DLOG("fd %d map size 0x%x", fd, sb.st_size);
    return sb.st_size;
}

static void *mapResource(int resfd, int barSize) {
    /* Map PCI resource region */
    void *map_addr = mmap(NULL,
                          barSize,
                          PROT_READ | PROT_WRITE,
                          MAP_SHARED,
                          resfd,
                          0);

    if (map_addr == MAP_FAILED) {
        DLOG("error mapping memory: %s\n", strerror(errno));
        close(resfd);
        return (void *) nullptr;
    }
    DLOG("mmap pointer: %p\n", map_addr);
    return map_addr;
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_map(
        JNIEnv* env,
        jobject,
        jstring resourceBase) {
    const char* baseString = env->GetStringUTFChars(resourceBase, nullptr);
    char bar0String[1024];
    char bar2String[1024];
    strcpy(bar0String, baseString);
    strcpy(bar2String, baseString);
    strcat(bar0String, "/resource0");
    strcat(bar2String, "/resource2");

    DLOG("bar0String %s", bar0String);
    DLOG("bar2String %s", bar2String);

    res0fd = openResource(bar0String);
    int size = barSize(res0fd);
    map0 = (int *) mapResource(res0fd, size);

    res2fd = openResource(bar2String);
    size = barSize(res2fd);
    map2 = (int *) mapResource(res2fd, size);

    env->ReleaseStringUTFChars(resourceBase, baseString);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_viken_pcie2_MainActivity_readIntReg(
        JNIEnv* env,
        jobject, /* this */
        jint regOffset) {
    int off = regOffset;
    int *ptr = (int *) map0 + off;
    return (*ptr);
}

extern "C" JNIEXPORT jshort JNICALL
Java_com_viken_pcie2_MainActivity_readShortReg(
        JNIEnv* env,
        jobject, /* this */
        jint regOffset) {
    int off = regOffset;
    short *ptr = (short *) map0 + off;
    return (*ptr);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_viken_pcie2_MainActivity_readIntMem(
        JNIEnv* env,
        jobject, /* this */
        jint regOffset) {
    int off = regOffset;
    int *ptr = (int *) map2 + off;
    return (*ptr);
}

extern "C" JNIEXPORT jshort JNICALL
Java_com_viken_pcie2_MainActivity_readShortMem(
        JNIEnv* env,
        jobject, /* this */
        jint regOffset) {
    int off = regOffset;
    short *ptr = (short *) map2 + off;
    return (*ptr);
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_writeIntMem(
        JNIEnv* env,
        jobject, /* this */
        jint data,
        jint regOffset) {
    int off = regOffset;
    int *ptr = (int *) map2 + off;
    *ptr = data;
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_writeShortMem(
        JNIEnv* env,
        jobject, /* this */
        jshort data,
        jint regOffset) {
    int off = regOffset;
    short *ptr = (short *) map2 + off;
    *ptr = data;
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_test(
        JNIEnv* env,
        jobject,
        jint data) {
    short *test = (short *) map0 + 0;
    short *wptr = (short *) map0 + 0x50;
    short *tptr = (short *) map0 + 0x8;
    short *sysStat = (short *) map0 + 0x80;
    short *detCtrl = (short *) map0 + 0xa1;
    short *opStat = (short *) map0 + 0xc;

    DLOG("date 0x%x, time 0x%x", *((short *) map0 + 5), *((short *) map0 + 4));

    *sysStat = 0x3;
    usleep(1000);
    *sysStat = 0x7;
    usleep(1000);
    *detCtrl = 1;

    DLOG("sys status 0x%x, detCtrl 0x%x, opStat 0x%x", *sysStat, *detCtrl, *opStat);
    sleep(1);

    short timer = *tptr;
    short lval = 0;
    int cnt = 0;
    DLOG("test start 0x%x, 0x%04hx, 0x%04hx", *test, timer, *wptr);
    for (int i = 0; i < 10000000; i++) {
        short val = *wptr;
        //if (timer != *tptr) DLOG("tptr diff 0x%x", *tptr);
//        if (val != lval)
//            DLOG("*wptr 0x%04hx, cnt %d", val, cnt);
        if (val < lval)
            DLOG("*wptr 0x%04hx, lval 0x%x, cnt %d", val, lval, cnt);
        //if (val == lval)
        //    DLOG("*wptr 0x%04hx, lval 0x%x, cnt %d", val, lval, cnt);
        //DLOG("val 0x%x", val);
        //usleep(1000);
        lval = val;
        cnt++;
    }
    DLOG("sys status 0x%x, detCtrl 0x%x", *sysStat, *detCtrl);
    *detCtrl = 0;
    *sysStat = 0x3;
    DLOG("sys status 0x%x, detCtrl 0x%x", *sysStat, *detCtrl);
}

static bool starting = true;
static int rptr = 0;
static int pixelsPerFpgaBuffer = 0x8000;
static uint64_t stime;
//static short *iptr = new short[12000];
short *readBunch(int pixels) {
    short *iptr = new short[pixels];
    unsigned short *wptr = (unsigned short *) map0 + 0x50;
    int nextCnt;
    int cntA;
    int ptrNow;
    int ptrFst = -1;
    int ptrPNZ = -1;
    int ptrANZ = -1;
    int zcnt = 0;
    bool wrap = false;
    bool wrap2 = false;
    uint64_t ptime = stime;
    stime = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
    memset(iptr, 0xff, pixels * sizeof(short));

    nextCnt = rptr + pixels;  // greater than the last pixel
    if (nextCnt >= pixelsPerFpgaBuffer) {
        nextCnt = nextCnt - pixelsPerFpgaBuffer;
        wrap = true;
        wrap2 = true;
    }
    bool wrapBounce = false; int bounceCnt = 10;
    bool waiting = true;
    int loopCnt = 0;
    while (waiting) {
        ptrNow = *wptr << 1; // read the current fpga write pointer, 32 bit address convert to 16 bit address
        if (ptrFst == -1) ptrFst = ptrNow;
        if (ptrNow != 0 && ptrPNZ == -1) ptrPNZ = ptrNow;
        if (ptrNow == 0) { zcnt++; DLOG("mah ZERO wptr, ptrPNZ %d loopCnt %d", ptrPNZ, loopCnt);/**/ }
        if (wrap) {
            if (ptrNow < rptr) {
                //DLOG("mah wrap @ loopCnt %d, ptrNow %d, nextCnt %d", loopCnt, ptrNow, nextCnt);
                wrapBounce = true;
                wrap = false; // wait until fpga write pointer to wrap
            }
        } else {
            if (starting) {  // get synced with fpga write pointer.
                if (ptrNow >= rptr && ptrNow < nextCnt)
                    starting = false;
            } else {
                //if (wrapBounce) { DLOG("mah ptrNow %d", ptrNow); if (bounceCnt-- == 0) wrapBounce = false; }
                if (ptrNow > nextCnt)
                    waiting = false; // wait until write pointer is greater than 12k pixels
            }
        }
        if (zcnt > 20)
            waiting = false; // too many zeros from register.  Rich has a "feature" in the fpga.
        loopCnt++;
    }

    // now copy data to buffer
    short *fpgaBuff = (short *) map2 + rptr;
    if (wrap2) {
        // copy in two pieces
        int cntA = pixels - nextCnt;
        memcpy(&iptr[0], &fpgaBuff[rptr], cntA * sizeof(short));
        memcpy(&iptr[cntA], &fpgaBuff[0], nextCnt * sizeof(short));
    } else {
        // copy int one piece
        memcpy(&iptr[0], &fpgaBuff[rptr], pixels * sizeof(short));
    }

    uint64_t etime = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
    DLOG("mah rptr %d, nextCnt %d, wrap2 %s, loopCnt %d, ptrNow %d, ptrFst %d, ptrPNZ %d, elapsed time %d, delta %lld",
         rptr, nextCnt, (wrap2) ? "true" : "false", loopCnt, ptrNow, ptrFst, ptrPNZ, etime - stime,
         stime - ptime);

    rptr = nextCnt;
    return iptr;
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_testProc(
        JNIEnv* env,
        jobject,
        jint data) {
    short *test = (short *) map0 + 0;
    short *tptr = (short *) map0 + 0x8;
    short *sysStat = (short *) map0 + 0x80;
    short *detCtrl = (short *) map0 + 0xa1;
    short *opStat = (short *) map0 + 0xc;
    int loopCnt = 10;
    short *buffers[loopCnt];

    DLOG("date 0x%x, time 0x%x", *((short *) map0 + 5), *((short *) map0 + 4));

    *sysStat = 0x3;
    usleep(1000);
    *sysStat = 0x7;
    usleep(1000);
    *detCtrl = 1;

    DLOG("sys status 0x%x, detCtrl 0x%x, opStat 0x%x", *sysStat, *detCtrl, *opStat);
    sleep(1);

    short timer = *tptr;
    short lval = 0;
    int cnt = 0;
    DLOG("test start 0x%x, 0x%04hx", *test, timer);
    for (int i = 0; i < loopCnt; i++) {
        short *data = readBunch(12000);
        buffers[i] = data;
        //delete data;
    }
    DLOG("sys status 0x%x, detCtrl 0x%x", *sysStat, *detCtrl);
    *detCtrl = 0;
    *sysStat = 0x3;
    DLOG("sys status 0x%x, detCtrl 0x%x", *sysStat, *detCtrl);

    // open file here
    FILE *f = fopen("output.csv", "w");
    int ppr = 1000;
    int rpb = 12;
    for (int i = 0; i < loopCnt; i++) {
        short *bptr = buffers[i];
        for (int j = 0; j < rpb; j++) {
            short *rptr = &bptr[j*ppr];
            for (int k = 0; k < ppr; k++) {
                if ((int) f > 1) fprintf(f, "%d, ", rptr[k]);
            }
            //if ((int) f > 1) fprintf(f, "\n");
        }
    }
    //if ((int) f > 1) fclose(f);
}

extern "C" JNIEXPORT void JNICALL
Java_com_viken_pcie2_MainActivity_starting(
        JNIEnv* env,
        jobject) {
    starting = true;
    rptr = 0;
}